﻿using System;
using System.Globalization;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine(DateTime.Now);
			Console.WriteLine(DateTime.Now.Date);
			Console.WriteLine(DateTime.Now.Day);
			Console.WriteLine(DateTime.Now.Month);
			Console.WriteLine(DateTime.Now.Year);
			Console.WriteLine(DateTime.Now.Hour);
			Console.WriteLine(DateTime.Now.Minute);
			Console.WriteLine(DateTime.Now.Second);

			Console.WriteLine(DateTime.Now.DayOfWeek);
			Console.WriteLine(DateTime.Now.DayOfYear); // içinde bulunduğun yılın kaçıncı günü

			Console.WriteLine(DateTime.Now.ToLongDateString());
			Console.WriteLine(DateTime.Now.ToShortDateString());
			Console.WriteLine(DateTime.Now.ToLongTimeString());
			Console.WriteLine(DateTime.Now.ToShortTimeString());

			Console.WriteLine(DateTime.Now.AddMonths(2));
			Console.WriteLine(DateTime.Now.AddYears(3));
			Console.WriteLine(DateTime.Now.AddDays(2));
			Console.WriteLine(DateTime.Now.AddHours(3));
			Console.WriteLine(DateTime.Now.AddSeconds(30));
			Console.WriteLine(DateTime.Now.AddMilliseconds(50));

      // Datetime Format
      Console.WriteLine(DateTime.Now.ToString("dd")); //Gün sayısı 
      Console.WriteLine(DateTime.Now.ToString("ddd")); //Gün ismi üç karakter
      Console.WriteLine(DateTime.Now.ToString("dddd")); //Gün ismi tam hali

      Console.WriteLine(DateTime.Now.ToString("MM")); //Ay sayısı
      Console.WriteLine(DateTime.Now.ToString("MMM")); //Ay ismi kısaltılmış
      Console.WriteLine(DateTime.Now.ToString("MMMM")); //Ay ismi tam hali

      Console.WriteLine(DateTime.Now.ToString("yy")); //Yıl sayısı iki karakter
      Console.WriteLine(DateTime.Now.ToString("yyyy")); //Yıl sayısı tam hali

      // Math Kütüphanesi
      Console.WriteLine("*******Math Kütüphanesi*********");
      Console.WriteLine(Math.Abs(-25)); // mutlak değer alır, içerisindeki değeri pozitif'e çevirir.
      Console.WriteLine(Math.Sin(10));
      Console.WriteLine(Math.Cos(10));
      Console.WriteLine(Math.Tan(10));

      Console.WriteLine(Math.Ceiling(22.3)); // Yukarı yuvarlar -> 23
      Console.WriteLine(Math.Round(22.3)); // Hangisine daha yakınsa -> 22
      Console.WriteLine(Math.Floor(22.7)); // Aşağı yuvarlar -> 22
      
      Console.WriteLine(Math.Max(2,6));
      Console.WriteLine(Math.Min(2,6));

      Console.WriteLine(Math.Pow(3,4)); // üs alma->3 üzeri 4= 81
      Console.WriteLine(Math.Sqrt(9)); // karekök alır -> 3
      Console.WriteLine(Math.Log(9)); // logaritmasını alır.
      Console.WriteLine(Math.Exp(3)); // e üzeri 3 verir.
      Console.WriteLine(Math.Log10(10)); // 10'un logaritma 10 tabanındaki karşılığı.

      string strDate = "07:05:45PM";
      // ParseExact = string bir tarih verisini date verisine çevirmek için kullanılır.
      // ParseExact kullanılan yerlerde System.Globalization'u sayfanın başında dahil etmek gerekir.
      var dt = DateTime.ParseExact(strDate, "hh:mm:sstt", CultureInfo.InvariantCulture);
      Console.WriteLine(dt.ToString("HH:mm:ss")); // Büyük harferle yazılmış HH 24 saat formatına göre saati yazar.


		}
	}
}