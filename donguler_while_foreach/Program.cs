﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // While Döngüs, Belirtilen şart sağlanana kadar çalışır
      // 1 den başlayarak girilen sayıda dahil olmak üzere tüm sayıların ortalamasını alan program
      // Console.Write("Lütfen bir sayı giriniz");
      // int ustSinir = int.Parse(Console.ReadLine());
      // int sayac = 1;
      // int toplam = 0;
      // while ( sayac <= ustSinir )
      // {
      //   toplam += sayac;
      //   sayac++;
      // }
      // Console.WriteLine("Ortalama=> " + toplam/ustSinir);

      // a'dan z'ye kadar olan hafrleri yazdır
      char character = 'a';
      while (character <= 'z')
      {
        if( character == 'w' || character == 'x' ){
          Console.Write(character+"(türkçe harf değil!), ");
        }else{
          Console.Write(character+", ");
        }
        character++;
      }

      Console.WriteLine("\n\n********* Foreach ***********");
      string[] arabalar = {"BMV","Ford","Toyota","Nissan"};
      foreach (var gezici in arabalar)
      {
        Console.WriteLine(gezici);
      }

		}
	}
}