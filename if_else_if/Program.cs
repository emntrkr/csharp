﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
			int time = DateTime.Now.Hour;
      Console.WriteLine("time=> "+time);

      if( time >= 6 && time < 11  ){
        Console.WriteLine("Günaydınn!");
      }
      else if ( time <= 18 )
      {
        Console.WriteLine("İyi Günler!");
      }
      else
      {
        Console.WriteLine("İyi Geceler!");
      }

      string sonuc = time >= 6 && time<=11 ? "Günaydınn!" : time <=18 ? "İyi Günler!" : "İyi Geceler!";
      Console.WriteLine(sonuc);
		}
	}
}