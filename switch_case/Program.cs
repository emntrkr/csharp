﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
			int month = DateTime.Now.Month;

      // Expression = kontrol edilmek istenen koşul
      switch (month)
      {
        case 1:{
          Console.WriteLine("Ocak ayındasınız");
          break;
        }
        case 2:{
          Console.WriteLine("Şubat ayındasınız");
          break;
        }
        case 4:{
          Console.WriteLine("Nisan ayındasınız");
          break;
        }
        case 3:{
          Console.WriteLine("Mart ayındasınız");
          break;
        }
        case 9:{
          Console.WriteLine("Eylül ayındasınız");
          break;
        }
        default:{
          Console.WriteLine("Hiçbir case'e girmediği durumda bu çalışacak");
          break;
        }
      }

      switch (month)
      {
        case 12:
        case 1:
        case 2:
          Console.WriteLine("Kış mevisimindesiniz.");
          break;
        case 3:
        case 4:
        case 5:
          Console.WriteLine("İlkbahar mevisimindesiniz.");
          break;
        case 6:
        case 7:
        case 8:
          Console.WriteLine("Yaz mevisimindesiniz.");
          break;
        case 9:
        case 10:
        case 11:
          Console.WriteLine("Sonbahar mevisimindesiniz.");
          break;
        default:
          break;
      }
		}
	}
}
          