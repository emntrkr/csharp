﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // int sayac;
      // try
      // {
      //   Console.WriteLine("Bir sayı giriniz");
      //   sayac = int.Parse(Console.ReadLine());
      // }
      // catch (ArgumentNullException ex)
      // {
      //   Console.WriteLine("Boş Değer Girdiniz.");
      //   // Console.WriteLine(ex);
      //   throw;
      // }
      // catch (FormatException ex)
      // {
      //   Console.WriteLine("Veri Tipi Uygun Değil.");
      //   // Console.WriteLine(ex);
      //   throw;
      // }
      // catch (OverflowException ex)
      // {
      //   Console.WriteLine("Veri tipinin sınırlarının dışında bir değer girdiniz.");
      //   // Console.WriteLine(ex);
      //   throw;
      // }

      // Ekrandan girilen sayıya kadar olan tek sayıları yazdır.
			// for (int i = 0; i <= sayac; i++)
      // {
      //   if( i % 2 == 1 ){
      //     Console.WriteLine(i);
      //   }
      // }

      // 1 ile 100 arasındaki sayıların tek olanlarının toplamı ile çift olanlarının toplamını yazdır.
      // int tekToplam = 0, ciftToplam = 0;
      // for (int i = 1; i <= 100; i++)
      // {
      //   if( i % 2 == 1 ){
      //     tekToplam += i;
      //   }else{
      //     ciftToplam += i;
      //   }
      // }
      // Console.WriteLine("1 ile 100 arasındaki tek sayıların toplamı => "+tekToplam);
      // Console.WriteLine("1 ile 100 arasındaki çift sayıların toplamı => "+ciftToplam);

      // break; ve continue;
      // break = döngüden çıkar
      // continue = o şartı sağlayan döngüyü es geçer, atlar.

      int n = 4;
      for (int y = n-1; y >= 0; y--)
      {
        for (int x = 0; x < n; x++)
        {
          if (x >= y){
            Console.Write("#");
          }else{
            Console.Write(" ");
          }
        }
        Console.WriteLine("");
      }
		}
	}
}
