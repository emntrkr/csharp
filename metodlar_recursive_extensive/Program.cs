﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      Recursive ornek = new Recursive(); // Aynı ifade = İslemler ornek = new(); olarakta yazılabilir.
			// Recursive - Öz Yinelemeli Fonksiyonlar
        // Kendi kendini çağıran fonksiyonlar - üs alma faktöriyel alma gibi işlemlerde kullanılabilir
        // İlkel Yöntem (For)
        int result = 1;
        for (int i = 1; i < 5; i++)
        {
          result = result * 3;
          Console.WriteLine(result);
        }
        Console.WriteLine(ornek.Expo(3,4));
      // Extensive - Yaygın Genişletilmiş Fonksiyonlar
        // Genişletme metodları bir tip (integer veya string vb.) üzerinde herhangi bir değişiklik yapmadan o tipi kolayca genişletmemize olanak sağlayan bir yapıdır

        string ifade = "Emin Trkr";
        bool sonuc = ifade.CheckSpaces();
        Console.WriteLine(sonuc);
        if(sonuc)
        {
          Console.WriteLine(ifade.RemoveWhiteSpaces());
        }
        Console.WriteLine(ifade.MakeUpperCase());
        Console.WriteLine(ifade.MakeLowerCase());

        int[] dizi = {9,3,6,2,5,0};
        dizi.SortArray();
        dizi.EkranaYazdir();

        int sayi = 5;
        Console.WriteLine(sayi.IsEven());

        Console.WriteLine(ifade.GetFirsCharacter());
		}
	}
  public class Recursive{
    public int Expo(int sayi, int us)
    {
      if( us < 2 ){
        return sayi;
      }
      return Expo(sayi,us-1)*sayi;
      /* ÇIKTI ADIMLARI: 
        Expo(3,3) * 3
        Expo(3,2) * 3 * 3
        Expo(3,1) * 3 * 3 * 3
        3 * 3 * 3 * 3
      */
    }
  }

  public static class Extensive{ //extensive classlar ve içerisindeki metodlar static olmalı, parametrelere ise this eklememiz gerekiyorki bunun bir extensive metod olduğunu bilsin.
    public static bool CheckSpaces(this string param)
    {
      return param.Contains(" ");
    }
    public static string RemoveWhiteSpaces(this string param)
    {
      string[] dizi = param.Split(" ");
      return string.Join("_",dizi);
    }
    public static string MakeUpperCase(this string param)
    {
      return param.ToUpper();
    }
    public static string MakeLowerCase(this string param)
    {
      return param.ToLower();
    }
    public static int[] SortArray(this int[] param){
      Array.Sort(param);
      return param;
    }
    public static void EkranaYazdir(this int[] param){
      foreach (var gezici in param)
      {
        Console.WriteLine(gezici);
      }
    }
    public static bool IsEven(this int param){
      return param%2 == 0;
    }
    public static string GetFirsCharacter(this string param){
      return param.Substring(0,1);
    }

  }
}