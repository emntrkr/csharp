﻿using System;
using System.Collections.Generic;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // Dictionary'ler ilk değeri key ikinci değeri de value için alırlar.
			Dictionary<int,string> kullanicilar = new Dictionary<int,string>();
      kullanicilar.Add(10,"Ayşe Kaya");
      kullanicilar.Add(12,"Ahmet Yılmaz");
      kullanicilar.Add(18,"Mehmet Yıldız");

      // Dizinin elemanlarına erişim
      Console.WriteLine("********** Elemanlara Erişim *************");
      Console.WriteLine(kullanicilar[12]);
      foreach (var item in kullanicilar)
      {
        Console.Write(item.Key + " " + item.Value);
        Console.WriteLine("");
      }

      // Count
      Console.WriteLine("********** Count *************");
      Console.WriteLine(kullanicilar.Count());

      // Contains
      Console.WriteLine("********** Contains *************");
      Console.WriteLine(kullanicilar.ContainsKey(12));
      Console.WriteLine(kullanicilar.ContainsValue("Mehmet Yıldız"));

      // Remove
      Console.WriteLine("********** Remove *************");
      Console.WriteLine(kullanicilar.Remove(12));

		}
	}
}