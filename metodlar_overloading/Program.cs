﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
			// out parametreler
      string sayi = "999";

      /* TryParse()
        Kullanıcıdan girilen değişkenin istediğimiz veri yapısında olmaması ihtimaline karşı kodun hata vermemesi için yapılır.
        İki parametresi var; ilki kontrol edilecek değişken yazılır, ikincisi ise kontrol sonucu dönen değeri bir değişkene atarız.
        TryParse geriye true veya false döner, yani tipi bool'dur.
      */
      /* out
        Metodlar klasöründeki ref ile hemen hemen aynıdır. İkisininde amacı fonksyion içinde bir değeri set etmek/değiştirmektir.
        Fonksiyon içerisinde return; kullanıp bir değer döndürmek yerine bu şekilde direk ilgili değişkenin değerini değiştirerek return yapmamıza gerek kalmaz.
        ref ve out arasındaki fark; ref tanımlanırken bir değeri olmak zorunda iken out tanımlarken bir değer girilmek zorunda değil.
      */
      bool sonuc = int.TryParse(sayi, out int outSayi);
      if(sonuc)
      {
        Console.WriteLine("Başarılı!");
        Console.WriteLine(outSayi);
      }else
      {
        Console.WriteLine("Başarısız!");
      }

      Metodlar nesne = new Metodlar();
      nesne.Topla(4,5, out int toplamSonuc);
      Console.WriteLine(toplamSonuc);

      // Metod Aşırı Yüklenme - Overloading - Bir fonksiyonu birden fazla şekilde kullanmak.
      int ifade = 999;
      /* EkranaYazdir() fonksiyonuna göndereceğimiz değer int, fonksyiondaki parametre string olduğu için convert yapmadan değeri gönderemiyoruz. Convertlemeden bu problemi çözmek istiyoruz..
        Aynı fonksiyonu parametrenin tipini int yaparak tekrar çağırdığımızda convert etmemize gerek kalmadan çalışır, bu olaya aşırı yükleme deniyor.
        Aşırı yükleme metod imzası denen şeyden oluşuyor;
          Metod İmzası = metotAdi + parametre sayısı + parametre tipi
          bu üç durumun aynı olmasında hata verir, ama bu 3 durumdan birisi bile farklı olan aynı adlı fonksyionlarda hata çıkartmaz.
      */
      nesne.EkranaYazdir(Convert.ToString(ifade)); 
      nesne.EkranaYazdir(ifade);
      nesne.EkranaYazdir("Emin","Türker");

      
      
		}
	}
  class Metodlar
  {
    public void Topla(int a, int b, out int toplam)
    {
      toplam = a+b;
    }

    // Aşırı Yükleme Örneği
    public void EkranaYazdir(string veri)
    {
      Console.WriteLine(veri);
    }
    public void EkranaYazdir(int veri)
    {
      Console.WriteLine(veri);
    }
    public void EkranaYazdir(string veri1, string veri2)
    {
      Console.WriteLine(veri1+veri2);
    }
  }
}