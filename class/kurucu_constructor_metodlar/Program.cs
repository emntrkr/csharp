﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // Açıklamalar
        // Bir sınıftan bir nesne oluşturulduğunda biz tanımlamasak bile arka planda çalışan varsayılan yapıcı metotlara kurucu yada constructor denir.
        // Sınıf nesnesi ilk oluşturulduğunda yapılmasını istediğimiz işleri kurucu metotlar içerisinde yaparız.
        // Varsayılan olarak constructor'ların tanımlanmadığındaki varsayılan görevi ilk değer ataması yapılmadığında onların default değerlerini set etmektir.
        // Şöyle düşünebiliriz; string veri tipinde bir propert varsa ve ona ilk değer atamasını yapmadığımızda, onun ilk değerini arka planda null olarak belirler.
        // Aynı durum int veri tipindeki property'lerde ise 0 olarak varsayılan ataması oluşturulur.
        // Constructor'ların oluşturulurken belirli şartları vardır;
          // İsmi class'ın ismi ile aynı olmak zorundadır.
          // Public olarak bildirilmeleri gerekir.
          // Geri dönüş değerleri yoktur. Bir istisna olarak kurucu metodlarda geri dönüş olmayanlarda verdiğimiz "void" geri dönüş tipi constractor metodlarda yazılmaz.        
      //

      Console.WriteLine("**********Çalışan 1**********");
      Calisan user1 = new Calisan("Ayşe","Kara",12345678,"İnsan Kaynakları");
      user1.CalisanBilgileri();
      
      Console.WriteLine("**********Çalışan 2**********");
      Calisan user2 = new Calisan();
      user2.Ad= "Deniz";
      user2.Soyad= "Arda";
      user2.Id= 87654321;
      user2.Departman= "Satın Alma";
      user2.CalisanBilgileri();
      
      Console.WriteLine("**********Çalışan 3**********");
      Calisan user3 = new Calisan("Emin","Türker");
      user3.CalisanBilgileri();
		}
	}

  class Calisan
  {
    public string Ad;
    public string Soyad;
    public int Id;
    public string Departman;

    // Aşağıda constractor metodu 3 kez çağırılmış. Çünkü yukarıdaki kodlarda Calisan sınıfının kullanımı 3 farklı şekilde kullanılmış.
    // Yani; ilk kullanımda class'a parametre verilmiş, ikinci kullanımda class'ın property'lerine erişip değer atamaları yapılmış, 3.kullanımda ise class'ın sadece Ad ve Soyad parametreleri verilmiş.
    // Bu 3 kullanımında hata olmaması için Class içerisindeki constructor'ı bu 3 kullanıma uygun olması için tekraren tanımladık.
    // Böylece daha önceki derslerde gördüğümüz metod overloading(metodun aşırı yüklenmesi) olayı yaşanmış oldu.
    // Normal bir class içerisinde tek bir constractor olması her zaman daha sağlıklıdır. Burada constructor'ın parametreli, hiç parametre almadan ve bazı parametreleri aldığı kullanım tipleri ile birlikte constructor metodunu aşırı yükleme örneği bir arada gösterilmiştir.
    // Bu üç duruma göre çağırıldığı yerlerde kullanımını da görmüş olduk. İhtiyaca göre constructor bu şekillerde kullanılabilir.

    public Calisan(string ad, string soyad, int id, string departman)
    {
      this.Ad = ad; //Ad = ad; bu şekilde de yazılabilirdi...
      this.Soyad = soyad;
      this.Id = id;
      this.Departman = departman;
    }

    public Calisan(string ad, string soyad)
    {
      this.Ad = ad; //Ad = ad; bu şekilde de yazılabilirdi...
      this.Soyad = soyad;
    }

    public Calisan(){}

    public void CalisanBilgileri()
    {
      Console.WriteLine("Çalışanın Adı:{0}", Ad);
      Console.WriteLine("Çalışanın Soyadı:{0}", Soyad);
      Console.WriteLine("Çalışanın Numarası:{0}", Id);
      Console.WriteLine("Çalışanın Departmanı:{0}", Departman);
    }
  }
}