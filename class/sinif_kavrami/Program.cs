﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // Söz Dizimi - Açıklamalar
        // class sinifAdi
        // {
        //   [Erişim Belirleyici] [Veri Tipi] OzellikAdi;
        //   [Erişim Belirleyici] [Geri Dönüş Tipi] MetodAdi([Parametre Listesi])
        //   {
        //     Metodun Komutları
        //   } 
        // }

        // Erişim Belirleyiciler : Erişim belirleyiciler önüne geldiği öğenin projenin nerelerinden erişilebileceğini belirler. Öğeleri korur gibi düşünebilirsiniz.
        // Public : Programın herhangi bir noktasından erişilebilir hale gelir.
        // Private: Sadece tanımlandığı sınıf içerisinde erişilebilen değişkenlere veya metodlara dönüştürür kodumuzu.
        // Internal: Sadece kendi bulunduğu proje içerisinde erişilebilen değişkenlere ve metodlara dönüştürür kodumuzu.
        // Protected: Sadece tanımlandığı sınıfta veya o sınıftan kalıtım/miras alan diğer sınıflarda kullanılabilir.
      //
      Calisan user1 = new Calisan();
      user1.Ad= "Ayşe";
      user1.Soyad= "Kara";
      user1.Id= 12345678;
      user1.Departman= "İnsan Kaynakları";
      user1.CalisanBilgileri();
      Console.WriteLine("****************");
      Calisan user2 = new Calisan();
      user2.Ad= "Deniz";
      user2.Soyad= "Arda";
      user2.Id= 87654321;
      user2.Departman= "Satın Alma";
      user2.CalisanBilgileri();
      
		}
	}

  class Calisan
  {
    public string Ad;
    public string Soyad;
    public int Id;
    public string Departman;

    public void CalisanBilgileri()
    {
      Console.WriteLine("Çalışanın Adı:{0}", Ad);
      Console.WriteLine("Çalışanın Soyadı:{0}", Soyad);
      Console.WriteLine("Çalışanın Numarası:{0}", Id);
      Console.WriteLine("Çalışanın Departmanı:{0}", Departman);
    }
  }



}