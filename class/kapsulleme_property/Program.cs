﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // Açıklama
        // Kapsüllemek: Sınıf içerisinde tanımlanan bir property veya metodları diğer class'lardan koruma işidir.
        // Bunu için biz erişim belirleyicilerden yararlanıyorduk(private,internal vb..).
        // Private yaparsak başka bir class'tan nesne oluşturulduğu zaman bu property/metod'a ulaşamayız.
        // Ama bazı durumlarda bu property/metod'u belirli koşullara göre açmak istiyoruz veya ona gelecek değeri kontrol ettirip ona göre bazı işlemler yaptırmak istiyoruz.
        // Bu gibi durumlarda kapsüllemelerden yararlanırız. Biz koşullara göre sınıfın üyelerini yani field'lerini kapatabiliriz veya değişmelerini engelleyebilriiz.
        // Bunun için property'leri kullanabiliriz. Property'ler get ve set'ten oluşuyor.
        // Get kısmında o property'inin değerini get eden, set kısmında ise o property'nin değerine bir şey set eden metod gibi düşünebiliriz
      //

      Ogrenci user1 = new Ogrenci();
      user1.Isim = "Ayşe";
      user1.Soyisim = "Yılmaz";
      user1.OgrNo = 171;
      user1.Sinif = 3;
      user1.sinifAtlat();
      user1.OgrenciBilgileriniGetir();

      Ogrenci user2 = new Ogrenci("Deniz","Arda",256,1);
      user2.sinifDusur();
      user2.sinifDusur();
      user2.OgrenciBilgileriniGetir();
		}
	}
  class Ogrenci
  {
    private string isim;
    private string soyisim;
    private int ogrNo;
    private int sinif;

    public string Isim
    { 
      get{ return isim; }
      set{ isim = value; }
    }
    public string Soyisim { get => soyisim; set => soyisim = value; }
    public int OgrNo { get => ogrNo; set => ogrNo = value; }
    public int Sinif
    { 
      get => sinif;
      set
      {
         if( value < 1 )
         {
            Console.WriteLine("Sınıf En Az 1 Olabilir!");
            sinif = 1;
         }
         else
         {
           sinif = value;
         }
      }
    }

    public Ogrenci(string isim, string soyisim, int ogrNo, int sinif)
    {
      Isim = isim;
      Soyisim = soyisim;
      OgrNo = ogrNo;
      Sinif = sinif;
    }
    public Ogrenci(){}
    
    public void OgrenciBilgileriniGetir()
    {
      Console.WriteLine("******* Öğrenci Bilgileri *******");
      Console.WriteLine("Öğrenci Adı:         {0}", Isim);
      Console.WriteLine("Öğrenci Soyadı:      {0}", Soyisim);
      Console.WriteLine("Öğrenci Numarası:    {0}", OgrNo);
      Console.WriteLine("Öğrenci Sınıfı:      {0}", Sinif);
    }

    public void sinifAtlat()
    {
      this.Sinif = this.Sinif + 1;
    }
    public void sinifDusur()
    {
      this.Sinif = this.Sinif - 1;
    }
  }
}