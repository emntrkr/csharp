﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // Açıklama
        // Uygulama geliştirirken sabit değerlerle çalışmak durumunda kalırız.
        // Bu noktalarda okunabilirliği yüksek bir program yazmak istiyorsak enum'lardan faydalanırız.
      //

			Console.WriteLine(Gunler.Pazar);
			Console.WriteLine((int)Gunler.Pazar);

      int sicaklikDeger = 32;
      if(sicaklikDeger <= (int)HavaDurumu.Normal)
        Console.WriteLine("Dışarıya çıkmak için havanın biraz daha ısınmasını bekleyelim");
      else if(sicaklikDeger >= (int)HavaDurumu.Sıcak)
        Console.WriteLine("Dışarıya çıkmak için çok sıcak bir gün");
      else if(sicaklikDeger >= (int)HavaDurumu.Sıcak && sicaklikDeger <= (int)HavaDurumu.Sıcak)
        Console.WriteLine("Dışarıya çıkılmak için yugun bir gün");
		}
	}
  enum Gunler
  {
    Pazartesi, //Pazartesi = 1 yazılırsa index normalde 0'dan başlarken artık 1'den başlar.
    Salı,
    Çarşamba,
    Perşembe,
    Cuma, //Cuma = 23 dersek Cumartesi 24, PAzar 25 olur. Yani hangi değeri veriresk ardışık olarak sıradakine o değeri verir.
    Cumartesi,
    Pazar
  }
  enum HavaDurumu
  {
    Soğuk = 5,
    Normal = 20,
    Sıcak = 25,
    CokSicak = 30
  }
}