﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // Açıklama
        // Struct'lar yani "yapılar" sınıflara çok benzerler. Struct ile yapıp sınıf ile yapamayacağız bir işlem yoktur diyebiliriz.
        // Class kullanmanızı gerektirecek kadar komplex olmayan yapılarınız varsa ve verileri kapsüllemek işinizi görecekse yapıları tercih edebilirsiniz.
        // Class lar referans tipli özellikler gösterir, Yapılar ise değer tipli özellikler gösterirler. En temel fark budur.
        // Diğer struct ya da sınıflardan kalıtım almazlar.
        // Interface'lerden kalıtım alabilirler.
        // new anahtar sözcüğü ile nesneleri yaratılabilir.
        // Sınıflar gibi metot, property ve field'lardan oluşurlar.
        // Sınıf içerisinde struct, struct içerisinde de sınıf oluşturulabilir.
        // Static üye barındırabilirler.
      //

      Dikdortgen orn1 = new Dikdortgen();
      orn1.kisaKenar = 3;
      orn1.uzunKenar = 4;
      Console.WriteLine("Class ile alan hesabı: {0}",orn1.AlanHesapla());

      // Struct kullanmanın diğer bir yazım yolu:
        // Dikdortgen_Struct orn2;
        // orn2.kisaKenar = 3;
        // orn2.uzunKenar = 4;
      //
      Dikdortgen_Struct orn2 = new Dikdortgen_Struct(3,4);
      
      Console.WriteLine("Struct ile alan hesabı: {0}",orn2.AlanHesapla());
		}
	}

  class Dikdortgen
  {
    public int kisaKenar;
    public int uzunKenar;
    public Dikdortgen()
    {
      kisaKenar = 3;
      uzunKenar = 4;
    }
    public long AlanHesapla()
    {
      return this.kisaKenar * this.uzunKenar;
    }
  }

  struct Dikdortgen_Struct
  {
    public int kisaKenar;
    public int uzunKenar;
    public Dikdortgen_Struct(int kisaKenar, int uzunKenar)
    {
      this.kisaKenar = kisaKenar;
      this.uzunKenar = uzunKenar;
    }
    public long AlanHesapla()
    {
      return this.kisaKenar * this.uzunKenar;
    }
  }
}