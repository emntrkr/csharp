﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{

      // Açıklama
        // Bir sınıfın static olamayan üyelerine nesneler aracılığıyla erişirken static olan metotlara ve özelliklere ise nesne oluşturmadan o sınıfın ismi aracılığıyla erişiriz.
        // Normal metotlar gibi kurucu metotları da static olarak tanımlayabiliriz. Sınıfın static üyelerinin başlangıç değerlerini static kurucular aracılığıyla yapabiliriz.
        // Parametre almazlar ve erişim belirleyicileri yoktur.
      //

			Console.WriteLine("Çalışan Sayısı:{0}",Calisan.CalisanSayisi);
      Calisan user1 = new Calisan("Ayşe","Yılmaz","IK");
			Console.WriteLine("Çalışan Sayısı:{0}",Calisan.CalisanSayisi);
      Calisan user2 = new Calisan("Ayşe2","Yılmaz2","IK");
      Calisan user3 = new Calisan("Ayşe3","Yılmaz3","IK");
			Console.WriteLine("Çalışan Sayısı:{0}",Calisan.CalisanSayisi);

      Console.WriteLine("Toplama İşlemi Sonucu: {0} ",Islemler.Topla(100,200));
      Console.WriteLine("Toplama İşlemi Sonucu: {0} ",Islemler.Cikar(400,300));
		}
	}

  class Calisan
  {
    private static int calisanSayisi;
    private string isim;
    private string soyisim;
    private string departman;

    public static int CalisanSayisi { get => calisanSayisi; }

    static Calisan(){
      calisanSayisi = 0;
    }

    public Calisan(string isim, string soyisim,  string departman)
    {
      this.isim = isim;
      this.soyisim = soyisim;
      this.departman = departman;
      calisanSayisi++; 
    }
  }

  static class Islemler
  {
    public static int Topla(int sayi1, int sayi2)
    {
      return sayi1 + sayi2;
    }
    public static int Cikar(int sayi1, int sayi2)
    {
      return sayi1 - sayi2;
    }
  }

}