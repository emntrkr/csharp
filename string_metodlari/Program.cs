﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
			string degisken = "Dersimiz CSharp, Hoşgeldiniz!";
			string degisken2 = "Dersimiz";
      
      // Length
      Console.WriteLine(degisken.Length);

      // ToUpper, ToLower
      Console.WriteLine(degisken.ToUpper());
      Console.WriteLine(degisken.ToLower());

      // Concat
      // Console.WriteLine(String.Concat(degisken, "Başlayalım mı?"));
      // CompareTo
      // degisken değeri degisken2 ile aynı ise 0; karakter uzunluğu daha düşükse -1, karakter uzunluğu daha büyükse 1 değerlerini döndürür. 
      Console.WriteLine(degisken.CompareTo(degisken2)); // 0,1,-1 döndürür.

      // Compare
      // 3.parametresindeki true/false değerleri büyük/küçük harflere duyarlılık durumunu anlatır. True ile büyük/küçük harf duyarlılığını dikkate alma demiş olduk
      Console.WriteLine(String.Compare(degisken,degisken2,true));

      // Contains
      Console.WriteLine(degisken.Contains(degisken2)); // degisken içerisinde degisken2 var mı? var ise true döner
      Console.WriteLine(degisken.EndsWith("Hoşgeldiniz!")); // degisken Hoşgeldiniz! ile bitiyor mu?
      Console.WriteLine(degisken.StartsWith("Başlayalım mı?")); // degisken Hoşgeldiniz! ile bitiyor mu?

      // IndexOf
      Console.WriteLine(degisken.IndexOf("CS")); //Belirtilen değeri arar, bulduğu ilk yerdeki karakterin index'ini verecek. Bulamazsa -1 verir.
      Console.WriteLine(degisken.LastIndexOf("i")); // Yazdığımız değeri degisken içerisinde arayıp bulduğu en sonuncusunun index'ini döner.

      // Insert
      Console.WriteLine(degisken.Insert(0,"Merhaba! ")); // Belirtilen indexten bşalayarak yazdığımız değeri ekler.

      // PadLeft, PadRight
      Console.WriteLine(degisken + degisken2.PadLeft(30)); // degisken2'nin soluna boşluk verir. Ama boşluk hesabını yaparken degisken2'nin karakter sayısınında buna dahil olduğu bilinmeli.
      Console.WriteLine(degisken + degisken2.PadLeft(30,'*')); // Boşluk yerine karakter ekleme olayı.
      Console.WriteLine(degisken.PadRight(50)); // Boşluk yerine karakter ekleme olayı.
      Console.WriteLine(degisken.PadRight(50,'-')); // Boşluk yerine karakter ekleme olayı.

      // Remove
      Console.WriteLine(degisken.Remove(10)); // 10.indexten itibaren sonuna kadar sil
      Console.WriteLine(degisken.Remove(5,3)); // 5.indexten başlayarak 3 karakter sil
      Console.WriteLine(degisken.Remove(0,1)); // 5.indexten başlayarak 3 karakter sil

      // Replace
      Console.WriteLine(degisken.Replace("CSharp","C#"));

      // Split
      Console.WriteLine(degisken.Split(' ')[1]); // Boşluk karakterine göre parçalara ayırıp dizilere atar. Daha sonrasında ayırılan dizinin 1.indisini getir.

      // Substring
      Console.WriteLine(degisken.Substring(4)); // 4.indexten başlayarak cümlenin sonuna kadar getirir.
      Console.WriteLine(degisken.Substring(4,6)); // 4.indexten başlayarak 6 karakter getirir.

		}
	}
}