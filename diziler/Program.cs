﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // Dizi Tanımlama Yolları
			string[] renkler = new string[5]; // 5 elemanlı değerleri henüz girilmemiş bir dizi tanımlaması
      string[] hayvanlar = {"kuş","köpek","kedi"}; // dizi tanımlanırken elemanları da tanımlanmış bir dizi tanımlaması
      int[] sayi; // eleman veya boyutu belirtilmemiş bir dizi tanımlaması
      sayi = new int[5]; // sayı adlı diziye boyut verildi.

      // Dizilere Eleman Atama 
      renkler[0] = "Mavi";
      sayi[3] = 10;

      Console.WriteLine(hayvanlar[1]);
      Console.WriteLine(sayi[3]);
      Console.WriteLine(renkler[0]);

      // Döngülerle Dizi Kullanımı
      // Klavyeden girilen n tane sayını ortalamasını hesaplamak
      Console.Write("Lütfen dizinin eleman sayısını giriniz: ");
      int diziUzunluk = int.Parse(Console.ReadLine());
      int [] sayiDizisi = new int[diziUzunluk];

      for (int i = 0; i < diziUzunluk; i++)
      {
        Console.WriteLine("Lütfen {0}. sayıyı giriniz", i+1);
        sayiDizisi[i] = int.Parse(Console.ReadLine());
      }

      int toplam = 0;
      foreach (var gezici in sayiDizisi)
      {
        toplam += gezici;
      }
      Console.WriteLine("Ortalama=> " + toplam/diziUzunluk);
		}
	}
}