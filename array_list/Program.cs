﻿using System;
using System.Collections;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
			ArrayList liste = new ArrayList();
      liste.Add("Ayşe");
      liste.Add(2);
      liste.Add(true);
      liste.Add('A');
      
      Console.WriteLine(liste[1]);
      foreach (var item in liste)
        Console.WriteLine(item);

      // AddRange
      Console.WriteLine("**** AddRange *******");
      string[] renkler = {"kırmızı","sarı","yeşil"};
      List<int> sayilar = new List<int>(){1,8,3,7,92,5};
      liste.AddRange(renkler);
      liste.AddRange(sayilar);

      foreach (var item in liste)
        Console.WriteLine(item);

      // Sort
      Console.WriteLine("********* Sort *********");
      liste.Sort(); // bu sıralama işlemi ArrayList içerisinde birden fazla türde değer olduğundan dolayı derlendiği zaman hata basacaktır. Sadece aynı tipte değer varsa o zaman .Sort() kullanılabilir 

		}
	}
}