﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // erisim_belirteci  geri_donustipi  metod_adi(parametreListesi / arguman)
      // {
      //    ...komutlar... 
      //    return; (eğer geriye bir şey döndüreceksek) 
      // }

      int a = 2;
      int b = 3;
      int sonuc = Topla(a,b);
      Console.WriteLine(sonuc);

      Metodlar nesne = new Metodlar();
      nesne.ekranaYazdir(Convert.ToString(a));
      nesne.ekranaYazdir(Convert.ToString(b));
      int sonuc2 = nesne.arttirVeTopla(ref a, ref b);
      // ref(referans) yazmak değişkenin değerini değiştirmektir. Şuan a ve b'nin değeri 1 arttırılması olarak kalıcı biçimde değiştirilmiş oldu.
      // Fonksiyon içerisinde bir değişiklik yapıldığında çağırıldığı yerdeki değişken de değişikliğe uğramış olur. Yani değeri değişir. Bu nedenle ref anahtar kelimesini kullanırken çok dikkatli olunmalıdır.
      nesne.ekranaYazdir(Convert.ToString(a));
      nesne.ekranaYazdir(Convert.ToString(b));
      nesne.ekranaYazdir(Convert.ToString(sonuc2));

		}
    static int Topla(int deger1, int deger2){
      return (deger1 + deger2);
    }
	}
  class Metodlar{
    public void ekranaYazdir(string veri){
      Console.WriteLine(veri);
    }

    public int arttirVeTopla(ref int deger1, ref int deger2)
    {
      deger1+=1;
      deger2+=1;
      return deger1+deger2;
    }
  }

}