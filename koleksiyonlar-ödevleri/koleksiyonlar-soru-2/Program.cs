﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      /*
        Klavyeden girilen 20 adet sayının en büyük 3 tanesi ve en küçük 3 tanesi bulan, 
        her iki grubun kendi içerisinde ortalamalarını alan ve bu ortalamaları ve ortalama toplamlarını console'a yazdıran 
        programı yazınız. (Array sınıfını kullanarak yazınız.)
      */
			int [] sayilar= new int[8];
      int [] enBuyuk = new int[3];
      int [] enKucuk = new int[3];

      for(int i = 0 ; i < 8; i++){
        Console.Write("{0}. sayıyı giriniz:", i+1);
        int sayi = Convert.ToInt32(Console.ReadLine());
        sayilar[i] = sayi;
      }

      foreach (var gezici in sayilar)
      {
        Console.WriteLine(gezici+"as");
      }

      Array.Sort(sayilar);
      for (int i = 0; i < 3; i++)
      {
        enKucuk[i] = sayilar[i];
      }
      Array.Reverse(sayilar);
      for (int i = 0; i < 3; i++)
      {
        enBuyuk[i] = sayilar[i];
      }

      Console.WriteLine("En küçük 3 sayının ortalaması = "+Math.Round(enKucuk.Average(),2));
      Console.WriteLine("En büyük 3 sayının ortalaması = "+Math.Round(enBuyuk.Average(),2));
      Console.WriteLine("En küçük ve en büyük ilk 3 sayıların ortalamalarının toplamı= " + Math.Round((enKucuk.Average()+enBuyuk.Average()),2));
		}
	}
}