﻿using System;
using System.Collections;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      /*
        Klavyeden girilen 20 adet pozitif sayının asal ve asal olmayan olarak 2 ayrı listeye atın. (ArrayList sınıfını kullanarak yazınız.)
        Negatif ve numeric olmayan girişleri engelleyin.
        Her bir dizinin elemanlarını büyükten küçüğe olacak şekilde ekrana yazdırın.
        Her iki dizinin eleman sayısını ve ortalamasını ekrana yazdırın.
      */
      int sayac = 0;
      ArrayList asal = new ArrayList();
      ArrayList asalDegil = new ArrayList();
			for (int i = 1; i <= 20; i++)
      {
        Console.WriteLine("{0}. Sayıyı Giriniz:", i);
        string str_sayi = Console.ReadLine();
        if(sayiSartlariSaglıyormu(str_sayi)){
          int sayi = Convert.ToInt32(str_sayi);
          for (int k = 2; k < sayi; k++)
          {
            if (sayi % k == 0)
            {
              sayac++;
            }
          }
          if (sayac == 0)
          {
            asal.Add(sayi);
          }
          else
          {
            asalDegil.Add(sayi);
          }
          sayac = 0;
        }else{
          Console.WriteLine("sayı şartı sağlamadı!, lütfen {0}. Sayıyı Giriniz:", i);
        }
      }
      int asalOrt = 0, asalDegilOrt = 0;
      asal.Sort();
      asal.Reverse();
      asalDegil.Sort();
      asalDegil.Reverse();
      Console.WriteLine("*** Asal Sayılar Listesi");
      foreach (var gezici in asal)
      {
        Console.Write(gezici + ", ");
        asalOrt += Convert.ToInt32(gezici);
      }
      Console.WriteLine("\n Asal Sayıların Ortalaması = "+ asalOrt/asal.Count );
      Console.WriteLine("\n **** Asal Olmayan Sayılar Listesi");
      foreach (var gezici in asalDegil)
      {
        Console.Write(gezici + ", ");
        asalDegilOrt += Convert.ToInt32(gezici);
      }
      Console.WriteLine("\n Asal Olmayan Sayıların Ortalaması = "+ asalDegilOrt/asalDegil.Count );
		}

    static bool sayiSartlariSaglıyormu(string gelenSayi)
    {
      bool sonuc = int.TryParse(gelenSayi, out int outSayi);
      if(sonuc)
      {
        if( outSayi > 2 ){
          // Console.WriteLine("true");
          return true;
        }
        else{
          Console.WriteLine("Lütfen sadece pozitif ve 1'den büyük bir sayı giriniz!");
          return false;
        }
      }else
      {
        Console.WriteLine("Lütfen sadece pozitif ve 1'den büyük bir sayı giriniz!");
        return false;
      }
      // return false;
    }

	}
}