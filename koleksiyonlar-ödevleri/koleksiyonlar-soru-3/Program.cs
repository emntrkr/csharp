﻿using System;
using System.Collections;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
    /*
      Klavyeden girilen cümle içerisindeki sesli harfleri bir dizi içerisinde saklayan 
      ve dizinin elemanlarını sıralayan programı yazınız.
    */
		static void Main(string[] args)
		{
      Console.WriteLine("Bir cümle giriniz:");
      string cumle = Console.ReadLine();
      string harfSesli = "aeıioöuü";
      ArrayList sesliHarfDizi = new ArrayList();
      string lowerCumle = cumle.ToLower();
      for (int i = 0; i < cumle.Length; i++)
      {
        if (harfSesli.Contains(lowerCumle[i]))
        {
          sesliHarfDizi.Add(lowerCumle[i]);
        }
      }
      sesliHarfDizi.Sort();
      foreach (var gezici in sesliHarfDizi)
      {
        Console.WriteLine(gezici);
      }
		}
	}
}