namespace interface_ornek_uygulama_abstract_eklemesi
{
  public class Civic : Otomobil
  {
    public override Marka HangiMarkaninAraci()
    {
      return Marka.Honda;
    }

    public override Renk StandartRengiNe()
    {
      return Renk.Gri;
    }
  }
}