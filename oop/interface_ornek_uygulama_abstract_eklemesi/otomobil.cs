using System;

namespace interface_ornek_uygulama_abstract_eklemesi
{
  public abstract class Otomobil{
    public int KacTekerlektenOlusur(){
      return 4;
    }
    public virtual Renk StandartRengiNe(){
      return Renk.Beyaz;
    }
    public abstract Marka HangiMarkaninAraci();
  }
}