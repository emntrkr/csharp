namespace interface_ornek_uygulama_abstract_eklemesi
{
  public class Corolla : Otomobil
  {
    public override Marka HangiMarkaninAraci()
    {
      return Marka.Toyota;
    }

    public int KacTekerlektenOlusur()
    {
      return 4;
    }

    public Renk StandartRengiNe()
    {
      return Renk.Gri;
    }
  }
}