using System;

namespace interface_ornek_uygulama_abstract_eklemesi
{
	public interface IOtomobil
  {
    int KacTekerlektenOlusur();
    Marka HangiMarkaninAraci();
    Renk StandartRengiNe();
  }
}