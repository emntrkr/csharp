﻿using System;

namespace interface_ornek_uygulama_abstract_eklemesi // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{

      Console.WriteLine("*******ABSTRACT********");

			Focus focus = new Focus();
      Console.WriteLine(focus.HangiMarkaninAraci().ToString());
      Console.WriteLine(focus.KacTekerlektenOlusur());
      Console.WriteLine(focus.StandartRengiNe().ToString());

      Civic civic = new Civic();
      Console.WriteLine(civic.HangiMarkaninAraci().ToString());
      Console.WriteLine(civic.KacTekerlektenOlusur());
      Console.WriteLine(civic.StandartRengiNe().ToString());
		}
	}
}