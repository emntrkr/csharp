﻿using System;

namespace interaface_arayuzler // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
			FileLogger fileLogger = new FileLogger();
      fileLogger.WriteLog();

      DbLogger dbLogger = new DbLogger();
      dbLogger.WriteLog();

      SmsLogger smsLogger = new SmsLogger();
      smsLogger.WriteLog();

      LogManager logManager = new LogManager(new FileLogger());
      logManager.WriteLog();
		}
	}
}