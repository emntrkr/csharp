﻿namespace MyApp // Note: actual namespace depends on the project name.
{
	public class Bitkiler:Canlilar
	{
    protected void Fotosentez(){
      Console.WriteLine("Bitkiler Fotosentez Yapar.");
    }
    public override void UyaranlaraTepkiVerme()
    {
      // base.UyaranlaraTepkiVerme();
      Console.WriteLine("Bitkiler Güneşe Tepki Verir.");
    }
  }
  public class TohumluBitkiler:Bitkiler
  {
    public TohumluBitkiler()
    {
      base.Beslenme();
      base.Bosaltim();
      base.Solunum();
      base.Fotosentez();
      base.UyaranlaraTepkiVerme();
    }

    public void TohumlaCogalma(){
      Console.WriteLine("Tohumlu Bitkiler Tohumla Çoğalır.");
    }
  }
  public class TohumsuzBitkiler:Bitkiler
  {
    public TohumsuzBitkiler()
    {
      base.Beslenme();
      base.Bosaltim();
      base.Solunum();
      base.Fotosentez();
    }
    public void SporlaCogalma(){
      Console.WriteLine("Tohumsuz Bitkiler Sporla Çoğalır.");
    }
  }
}