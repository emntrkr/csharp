﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      //                        CANLILAR
      //                           |
      //            BİTKİLER                   HAYVANLAR
      //          |         |               |             |
      //      TOHUMLU   TOHUMSUZ      SÜRÜNGENLER      KUŞLAR

			TohumluBitkiler patates = new TohumluBitkiler();
      patates.TohumlaCogalma();

      Console.WriteLine("*************");

      Kuslar marti = new Kuslar();
      marti.UcmaHareketi();
		}
	}
}