﻿namespace MyApp // Note: actual namespace depends on the project name.
{
	public class Hayvanlar:Canlilar
	{
    protected void Adaptasyon(){
      Console.WriteLine("Hayvanlar Adaptasyon Kurabilir.");
    }
    public override void UyaranlaraTepkiVerme()
    {
      base.UyaranlaraTepkiVerme();
      Console.WriteLine("Hayvanlar Temasa Tepki Verir.");
    }
  }
  public class Surungenler:Hayvanlar
	{
    public Surungenler()
    {
      base.Beslenme();
      base.Bosaltim();
      base.Solunum();
      base.Adaptasyon();
    }
    public void SurunmeHareketi(){
      Console.WriteLine("Sürüngenler Sürünerek Haraket Ederler.");
    }
  }
  public class Kuslar:Hayvanlar
	{
    public Kuslar()
    {
      base.Beslenme();
      base.Bosaltim();
      base.Solunum();
      base.Adaptasyon();
      base.UyaranlaraTepkiVerme();
    }
    public void UcmaHareketi(){
      Console.WriteLine("Kuşlar Uçarak Hareket Ederler..");
    }
  }
}