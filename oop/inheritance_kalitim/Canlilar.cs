﻿namespace MyApp // Note: actual namespace depends on the project name.
{
	public class Canlilar
	{
    protected void Beslenme()
    {
      Console.WriteLine("Canlılar Beslenir.");
    }
    protected void Solunum()
    {
      Console.WriteLine("Canlılar Solunum Yapar.");
    }
    protected void Bosaltim()
    {
      Console.WriteLine("Canlılar Boşaltım Yapar.");
    }
    public virtual void UyaranlaraTepkiVerme(){
      Console.WriteLine("Canlılar Uyaranlara Tepki Verir.");
    }
  }
}