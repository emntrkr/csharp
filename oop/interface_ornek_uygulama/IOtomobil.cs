using System;

namespace interface_ornek_uygulama // Note: actual namespace depends on the project name.
{
	public interface IOtomobil
  {
    int KacTekerlektenOlusur();
    Marka HangiMarkanınAraci();
    Renk StandartRengiNe();
  }
}