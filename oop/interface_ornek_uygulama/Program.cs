﻿using System;

namespace interface_ornek_uygulama // Note: actual namespace depends on the project name.
{
	internal class interface_ornek_uygulama
	{
		static void Main(string[] args)
		{
			Focus focus = new Focus();
      Console.WriteLine(focus.HangiMarkanınAraci().ToString());
      Console.WriteLine(focus.KacTekerlektenOlusur());
      Console.WriteLine(focus.StandartRengiNe().ToString());

      Civic civic = new Civic();
      Console.WriteLine(civic.HangiMarkanınAraci().ToString());
      Console.WriteLine(civic.KacTekerlektenOlusur());
      Console.WriteLine(civic.StandartRengiNe().ToString());

		}
	}
}