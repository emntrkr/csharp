﻿using System;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
      // Sort
			int[] sayiDizisi = {23,12,4,86,72,3,11,17};
      Console.WriteLine("Sırasız Dizi");
      foreach (var gezici in sayiDizisi)
      {
        Console.WriteLine(gezici);
      }
      Console.WriteLine("Sıralı Dizi");
      Array.Sort(sayiDizisi);
      foreach (var gezici in sayiDizisi)
      {
        Console.WriteLine(gezici);
      }

      // Clear - sıfır atama
      Array.Clear(sayiDizisi,2,2); // sayiDizisi elemanlarını kullanarak 2.indexten itibaren 2 tane elemanı sıfırlar.
      Console.WriteLine("Clear Dizi");
      foreach (var gezici in sayiDizisi)
      {
        Console.WriteLine(gezici);
      }

      // Reverse - tersine çevirme
      Console.WriteLine("Reverse Dizi");
      Array.Reverse(sayiDizisi);
      foreach (var gezici in sayiDizisi)
      {
        Console.WriteLine(gezici);
      }

      // IndexOf - Dizi içerisinde arama yapmak
      Console.WriteLine("IndexOf Dizi");
      Console.WriteLine(Array.IndexOf(sayiDizisi,23)); // sayiDizisindeki elemanlardan 23 değerinin kaçıncı indexte olduğunu bul

      // Resize - Yeniden Boyutlandırma
      Console.WriteLine("Resize Dizi");
      Array.Resize<int>(ref sayiDizisi,9);
      sayiDizisi[8] = 99;
      Console.WriteLine( sayiDizisi.Count() );

		}
	}
}