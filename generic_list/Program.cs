﻿using System;
using System.Collections.Generic;

namespace MyApp // Note: actual namespace depends on the project name.
{
	internal class Program
	{
		static void Main(string[] args)
		{
			// Kullanılış Biçimi: List<T> class
      // Sysytem.Collections.Generic
      // T => object türündedir.

      List<int> sayiListesi = new List<int>();
      sayiListesi.Add(10);
      sayiListesi.Add(4);
      sayiListesi.Add(5);
      sayiListesi.Add(92);
      sayiListesi.Add(34);

      List<string> renkListesi = new List<string>();
      renkListesi.Add("Kırmızı");
      renkListesi.Add("Mavi");
      renkListesi.Add("Turuncu");
      renkListesi.Add("Sarı");
      renkListesi.Add("Yeşil");

      // Count
      Console.WriteLine(renkListesi.Count); // listedeki eleman sayısı

      // Listeden eleman çıkarma
      renkListesi.Remove("Yeşil");
      renkListesi.RemoveAt(0);

      // Foreach ve List.ForEach ile listelere erişim
      foreach (var gezici in sayiListesi)
      {
        Console.WriteLine(gezici);
      }
      renkListesi.ForEach(renk=>Console.WriteLine(renk));

      // Liste içerisinde arama
      if(sayiListesi.Contains(10)){
        Console.WriteLine("10 liste içerisinde bulundu");
      }

      // Eleman ile index'ine erişebilmek
      renkListesi.Sort();
      Console.WriteLine(renkListesi.BinarySearch("Sarı"));

      // Diziyi List'e Çevirme
      string[] hayvanlar = {"kedi","köpek","kuş"};
      List<string> hayvanListesi = new List<string>(hayvanlar);

      // Liste Boşaltmak
      hayvanListesi.Clear();

      // Liste içerisinde nesne tutmak
      List<Kullanicilar> kullaniciListesi = new List<Kullanicilar>();

      Kullanicilar kullanici1 = new Kullanicilar();
      kullanici1.Isim = "Emin";
      kullanici1.Soyisim = "Trkr";
      kullanici1.Yas = 26;

      Kullanicilar kullanici2 = new Kullanicilar();
      kullanici2.Isim = "Ayşe";
      kullanici2.Soyisim = "Yılmaz";
      kullanici2.Yas = 22;

      kullaniciListesi.Add(kullanici1);
      kullaniciListesi.Add(kullanici2);

      List<Kullanicilar> yeniListe = new List<Kullanicilar>();
      yeniListe.Add(new Kullanicilar(){
        Isim = "Ahmet",
        Soyisim = "Yıldız",
        Yas = 23
      });

      foreach (var gezici in kullaniciListesi)
      {
        Console.WriteLine("Kullanıcı Adı: " + gezici.Isim);
        Console.WriteLine("Kullanıcı Soyadı: " + gezici.Soyisim);
        Console.WriteLine("Kullanıcı Yaşı: " + gezici.Yas);
      }

		}
	}
  public class Kullanicilar
  {
    string isim;
    string soyisim;
    int yas;

    public string Isim { get => isim; set => isim = value; }
    public string Soyisim { get => soyisim; set => soyisim = value; }
    public int Yas { get => yas; set => yas = value; }
  }
}